//! # reminder
//! This is a largely based off of matrix_bot_api: https://github.com/zwieberl/matrix_bot_api
extern crate fractal_matrix_api;
extern crate chrono;
extern crate reminderlib;
extern crate lingqbotlib;
extern crate regex;
extern crate config;

use chrono::prelude::*;
use config::Config;
use fractal_matrix_api::backend::{Backend, BKCommand, BKResponse};
use fractal_matrix_api::types::{Room, Message};
use fractal_matrix_api::types::message::get_txn_id;
use regex::{Regex};

use std::fs;
use std::{thread, time};
use std::collections::HashMap;
use std::sync::{RwLock, Arc};
use std::sync::mpsc::channel;
use std::sync::mpsc::{Sender, Receiver};

// Also could be "m.notice" if you want
const MSG_TYPE: &str = "m.text";

pub struct MatrixBot {
    backend: Sender<BKCommand>,
    rx: Receiver<BKResponse>,
    uid: Arc<RwLock<String>>,
    rbots: HashMap<String, reminderlib::Bot>,
    lingqbot: Option<lingqbotlib::Bot>,
    signed_in: bool,
    verbose: bool,
    post_message_sleep: u64,
    settings: Config,
}

impl MatrixBot {
    /// Consumes any struct that implements the MessageHandler-trait.
    pub fn new(config: Config) -> MatrixBot {
        let (tx, rx): (Sender<BKResponse>, Receiver<BKResponse>) = channel();
        let bk = Backend::new(tx);

        // Here it would be ideal to extend fractal_matrix_api in order to be able to give
        // sync a limit-parameter.
        // Until then, the workaround is to send "since" of the backend to "now".
        // Not interested in any messages since login
        bk.data.lock().unwrap().since = Some(Local::now().to_string());
        MatrixBot {
            backend: bk.run(),
            rx: rx,
            uid: Arc::new(RwLock::new(String::default())),
            rbots: HashMap::new(),
            lingqbot: None,
            signed_in: false,
            verbose: false,
            post_message_sleep: 0,
            settings: config
        }
    }

    /// If true, will print all Matrix-message coming in and going out (quite verbose!) to stdout
    /// Default: false
    pub fn set_verbose(&mut self, verbose: bool) {
        self.verbose = verbose;
    }

    /// Blocking call that runs as long as the Bot is running.
    /// Will call for each incoming text-message the given MessageHandler.
    /// Bot will automatically join all rooms it is invited to.
    /// Will return on shutdown only.
    /// All messages prior to run() will be ignored.
    pub fn run(mut self, user: &str, password: &str, homeserver_url: &str) {
        self.backend
            .send(BKCommand::Login(
                user.to_string(),
                password.to_string(),
                homeserver_url.to_string(),
            ))
            .unwrap();
        loop {
            let cmd = self.rx.recv().unwrap();
            if !self.handle_recvs(cmd) {
                break;
            }
        }
    }

    /// Will shutdown the bot. The bot will not leave any rooms.
    pub fn shutdown(&self) {
        self.backend.send(BKCommand::ShutDown).unwrap();
    }

    /// Will leave the given room (give room-id, not room-name)
    pub fn leave_room(&self, room_id: &str) {
        self.backend
            .send(BKCommand::LeaveRoom(room_id.to_string()))
            .unwrap();
    }

    /// Sends a message to a given room, with a given message-type.
    ///  * msg:     The incoming message
    ///  * room:    The room-id, the message should be sent to
    ///  * msgtype: Type of message (text or notice)
    pub fn send_message(&self, msg: &str, room: &str) {
        let uid = self.uid.clone();
        let uid_value = uid.read().unwrap();
        send_message_to_backend(&self.backend, uid_value.clone(), msg, room, self.verbose);
    }

    /* --------- Private functions ------------ */
    fn spin_up_bots(&mut self) {
        // Make sure we have our rooms directory
        let _        = fs::create_dir("./rooms");
        let paths    = fs::read_dir("./rooms").unwrap();
        let db_regex = Regex::new(r"([^/]+)\.db$").unwrap(); // NOTE probably breaks on Windows

        // Find all .db files in the rooms directory
        for direntry in paths {
            let direntry = direntry.unwrap();
            let path     = format!("{}", direntry.path().display());

            if let Some(caps) = db_regex.captures(&path) {
                let room = caps.get(1).unwrap().as_str().to_string();
                println!("Loading {} as {}", path, room);

                // Add reminder bot
                let (bot, chats) = reminderlib::Bot::new(&path).expect("Failed to start reminder bot");

                self.spawn_chat_sender_thread(&room, chats);
                self.rbots.insert(room.clone(), bot);
            }
        }

        // Add lingq bot
        let api_key = self.settings.get_str("lingq_api_key").unwrap();
        let collection = self.settings.get_int("lingq_collection").unwrap();
        self.lingqbot = Some(lingqbotlib::Bot::new(&api_key, collection));

        // Misc setup
        self.post_message_sleep = self.settings.get_int("post_message_sleep").unwrap_or(0) as u64;
    }

    fn create_rbot_for_room(&mut self, room: String) -> &mut reminderlib::Bot {
        let db_path = format!("./rooms/{}.db", room);
        let (bot, chats) = reminderlib::Bot::new(&db_path).expect("Failed to start new bot");
        let key = room.clone();

        println!("Initializing reminder bot for {}", room);

        self.spawn_chat_sender_thread(&room, chats);
        self.rbots.insert(room, bot);
        self.rbots.get_mut(&key).unwrap()
    }

    fn spawn_chat_sender_thread(&self, room: &String, chats: Receiver<String>) {
        let new_backend = self.backend.clone();
        let new_room    = room.clone();
        let new_uid     = self.uid.clone();

        thread::spawn(move || send_chats(chats, new_backend, new_room, new_uid));
    }


    fn handle_recvs(&mut self, resp: BKResponse) -> bool {
        if self.verbose {
            println!("<=== received: {:?}", resp);
        }

        match resp {
            BKResponse::UpdateRooms(x) => self.handle_rooms(x),
            BKResponse::RoomMessages(x) => self.handle_messages(x),
            BKResponse::Token(new_uid, _, _) => {
                let uid_ref = self.uid.clone();
                let mut uid_ref_guard = uid_ref.write().unwrap();
                *uid_ref_guard = new_uid;
                self.backend.send(BKCommand::Sync(None, true)).unwrap();

                if !self.signed_in {
                    self.signed_in = true;
                    self.spin_up_bots();
                }
            }
            BKResponse::Sync(_) => self.backend.send(BKCommand::Sync(None, false)).unwrap(),
            BKResponse::SyncError(_) => self.backend.send(BKCommand::Sync(None, false)).unwrap(),
            BKResponse::ShutDown => {
                return false;
            }
            _ => (),
        }
        true
    }

    fn uid_matches(&self, other_uid: &str) -> bool {
        let uid = self.uid.read().unwrap();
        *uid == other_uid
    }

    fn handle_messages(&mut self, messages: Vec<Message>) {
        for message in messages {
            /* First of all, mark all new messages as "read" */
            self.backend
                .send(BKCommand::MarkAsRead(
                    message.room.clone(),
                    message.id.clone(),
                ))
                .unwrap();
            
            if self.uid_matches(&message.sender) { continue }

            if !self.try_handle_reminder(&message) && !self.try_handle_lingq(&message) {
                self.send_message("Sorry, didn't understand that!", &message.room);
            }

            // This is a hack because sometimes the message queue doesn't actually seem to
            // flush by the time we're done handling easy messages...
            if self.post_message_sleep > 0 {
                thread::sleep(time::Duration::from_secs(self.post_message_sleep));
            }
        }
    }

    fn try_handle_reminder(&mut self, message: &Message) -> bool {
        let room = message.room.clone();

        // Get or create the bot
        let bot = match self.rbots.get_mut(&message.room) {
            Some(b) => b,
            None    => self.create_rbot_for_room(room)
        };

        // See if this message is actually a request for a reminder
        let reminder = match bot.parse_message(&message.body) {
            Some(r) => r,
            None    => return false
        };

        // Build reply message
        let reply = match bot.set_reminder(reminder.clone()) {
             Ok(())   => format!("Saved reminder for {}", reminder.trigger_time().to_rfc2822()),
             Err(msg) => format!("Failed to set reminder. {} I'm probably broken.", msg)
        };

        // Send reply and we're done
        self.send_message(&reply, &message.room);
        true
    }

    fn try_handle_lingq(&mut self, message: &Message) -> bool {
        // The lingq bot should already be initialized
        let lingqbot = match &mut self.lingqbot {
            Some(bot) => bot,
            None      => {
                self.send_message("The LingQ bot isn't actually initialized yet...", &message.room);
                return false;
            }
        };

        // See if this message is good to be turned into a lesson
        let lesson = match lingqbot.parse_message(&message.body) {
            Some(l) => l,
            None    => return false
        };

        // Get that lesson URL!
        let reply = match lingqbot.create_lesson(lesson) {
            Some(url) => format!("Here you go: {}", url),
            None      => String::from("Don't know what happened, but I couldn't create the lesson. Sorry! Maybe just go here https://www.lingq.com/en/learn/ja/import/contents/?add")
        };

        // Send reply and we're done
        self.send_message(&reply, &message.room);
        true
    }

    fn handle_rooms(&self, rooms: Vec<Room>) {
        for rr in rooms {
            if rr.membership.is_invited() {
                self.backend
                    .send(BKCommand::JoinRoom(rr.id.clone()))
                    .unwrap();
                println!("Joining room {}", rr.id.clone());
            }
        }
    }
}


fn send_chats(chat_source: Receiver<String>, chat_dest: Sender<BKCommand>, room: String, uid: Arc<RwLock<String>>) {
    loop {
        let chat = chat_source.recv().expect("Chat receiver channel closed");

        {
            let uid_ref = uid.read().unwrap();
            let uid = uid_ref.clone();
            send_message_to_backend(&chat_dest, uid, &chat, &room, true);
        }
    }
}


fn send_message_to_backend(backend: &Sender<BKCommand>, uid: String, msg: &str, room: &str, verbose: bool) {
    let date = Local::now();

    let m = Message {
        sender: uid.to_string(),
        mtype: MSG_TYPE.to_string(),
        body: msg.to_string(),
        room: room.to_string(),
        date: Local::now(),
        thumb: None,
        url: None,
        id: get_txn_id(room, msg, &date.to_string()),
        formatted_body: None,
        format: None,
        in_reply_to: None,
        receipt: std::collections::HashMap::new(),
        redacted: false,
        extra_content: None,
        source: None
    };

    if verbose {
        println!("===> sending: {:?}", m);
    }
    backend.send(BKCommand::SendMsg(m)).unwrap();
}


fn main() {
    let mut settings = Config::default();
    settings.merge(config::File::with_name("bot.toml")).unwrap();

    let user = settings.get_str("user").unwrap();
    let password = settings.get_str("password").unwrap();
    let homeserver_url = settings.get_str("homeserver_url").unwrap();

    let bot = MatrixBot::new(settings);
    bot.run(&user, &password, &homeserver_url);
}
